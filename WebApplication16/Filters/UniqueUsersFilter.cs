﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApplication16.Filters
{
	public class UniqueUsersFilter : Attribute, IActionFilter
	{
		private static HashSet<string> _uniqueUsers = new HashSet<string>();
		public void OnActionExecuted(ActionExecutedContext _)
		{
		}

		public void OnActionExecuting(ActionExecutingContext context)
		{
			if (context.HttpContext.Request.Cookies.TryGetValue("UniqeKey", out var uniqeKey))
			{
				return;
			}
			else
			{
				var rand = new Random();
				int firstNum = rand.Next(0, 100);
				int secondNum = rand.Next(firstNum, 1000);

				uniqeKey = $"{firstNum}{secondNum}{firstNum}";
				_uniqueUsers.Add(uniqeKey);
				string path = Path.Combine(Directory.GetCurrentDirectory(), "uniqeUsers.txt");
				File.WriteAllText(path, $"Users count: {_uniqueUsers.Count}");
				context.HttpContext.Response.Cookies.Append("UniqeKey", uniqeKey);
			}
			
				
		}
	}
}
