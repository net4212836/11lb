﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace WebApplication15.Filters
{
	public class LoggerFilter : Attribute, IActionFilter
	{
		public void OnActionExecuted(ActionExecutedContext _)
		{
		}

		public void OnActionExecuting(ActionExecutingContext context)
		{
			string path = Path.Combine(Directory.GetCurrentDirectory(), "logger.txt");
			string? actionName = context.ActionDescriptor.DisplayName;
			File.AppendAllText(path, $"{actionName}: {DateTime.Now.ToString()}\n");
		}
	}
}
